---
layout: post
title:  "#0000 - rapo + proyectocoso"
date:   2021-02-25 20:27:10 -0300
categories: post
image: '/assets/images/2021-02-25/clau_demeter.jpg'
---

Improvisación de rapo + proyectocoso (clau brito) en Demeter, dentro de la semana que fue el festival Danzafuera.

[https://grumel.bandcamp.com/album/rapo-proyectocoso-danzafuera-demeter](https://grumel.bandcamp.com/album/rapo-proyectocoso-danzafuera-demeter)

![01](/assets/images/2021-02-25/clau_demeter.jpg)
