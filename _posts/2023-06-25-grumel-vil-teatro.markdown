---
layout: post
title:  "#0022 - G.R.U.M.E.L VIL Teatro"
date:   2023-06-25 10:30:10 -0300
categories: post
image: '/assets/images/2023-06-25/flyer.jpg'
---

# G.R.U.M.E.L | VIL Teatro 25/6 | 18hs

* Pablo Díaz & Braulio

@p.ablod.iaz & @_rvrls

* Sucia y Seca & Gustavo Caccavo

@suciayseca & @gustavocaccavo

* Sarah Connor

@drahgabriel #sergioperalta @seba.scianca

* Flyer @paco.plastilina

* Iluminación @svgui_

* Comida y barra

* Anticipadas a $1200 en @la_disqueria

![01](/assets/images/2023-06-25/flyer.jpg)

<br>
![01](/assets/images/2023-06-25/1.jpg)
![01](/assets/images/2023-06-25/2.jpg)
![01](/assets/images/2023-06-25/3.jpg)
![01](/assets/images/2023-06-25/4.jpg)
![01](/assets/images/2023-06-25/5.jpg)
![01](/assets/images/2023-06-25/6.jpg)
![01](/assets/images/2023-06-25/7.jpg)
![01](/assets/images/2023-06-25/8.jpg)
![01](/assets/images/2023-06-25/9.jpg)
![01](/assets/images/2023-06-25/10.jpg)
![01](/assets/images/2023-06-25/11.jpg)
![01](/assets/images/2023-06-25/12.jpg)
![01](/assets/images/2023-06-25/13.jpg)
![01](/assets/images/2023-06-25/14.jpg)
![01](/assets/images/2023-06-25/15.jpg)
![01](/assets/images/2023-06-25/16.jpg)


<video controls>
  <source src="/assets/videos/2023-06-25/v1.MOV" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2023-06-25/v2.MOV" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2023-06-25/v3.MOV" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2023-06-25/v4.MOV" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2023-06-25/v5.MOV" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2023-06-25/v6.MOV" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2023-06-25/v7.MOV" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2023-06-25/v8.MOV" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2023-06-25/v9.MOV" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2023-06-25/v10.MOV" type="video/mp4">
</video>
