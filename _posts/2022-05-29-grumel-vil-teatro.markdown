---
layout: post
title:  "#0009 - Jornada G.R.U.M.E.L"
date:   2022-05-29 02:46:10 -0300
categories: post
image: '/assets/images/2022-05-29/flayer.jpg'
---

# Jornada G.R.U.M.E.L | VIL Teatro

![01](/assets/images/2022-05-29/flayer.jpg)<br>
<br>

-> [La Alfreda], de Asturias & C.A.O.S a GRUMEL

-> [Paco & Luca]

-> [Savi4 Flor & Max Jering4]

-> [El Hexágono]

<br>
𝐅𝐋𝐘𝐄𝐑 𝐩𝐨𝐫 𝐑𝐞𝐠𝐢𝐧𝐚 𝐑𝐚𝐟𝐟𝐞𝐭𝐭𝐨
<br>
[https://neuenumeral.com](https://neuenumeral.com)

<hr>
Fotos por [Felicitas PH](https://www.instagram.com/felisisti.ph/)

![01](/assets/images/2022-05-29/1.jpg)
![01](/assets/images/2022-05-29/2.jpg)
![01](/assets/images/2022-05-29/3.jpg)
![01](/assets/images/2022-05-29/4.jpg)
![01](/assets/images/2022-05-29/5.jpg)
![01](/assets/images/2022-05-29/6.jpg)
![01](/assets/images/2022-05-29/7.jpg)
![01](/assets/images/2022-05-29/8.jpg)
![01](/assets/images/2022-05-29/9.jpg)
![01](/assets/images/2022-05-29/10.jpg)
![01](/assets/images/2022-05-29/11.jpg)
![01](/assets/images/2022-05-29/12.jpg)
![01](/assets/images/2022-05-29/13.jpg)
![01](/assets/images/2022-05-29/14.jpg)
![01](/assets/images/2022-05-29/15.jpg)
![01](/assets/images/2022-05-29/16.jpg)
![01](/assets/images/2022-05-29/17.jpg)
![01](/assets/images/2022-05-29/18.jpg)
![01](/assets/images/2022-05-29/19.jpg)
![01](/assets/images/2022-05-29/20.jpg)
![01](/assets/images/2022-05-29/21.jpg)
![01](/assets/images/2022-05-29/22.jpg)
![01](/assets/images/2022-05-29/23.jpg)
![01](/assets/images/2022-05-29/24.jpg)
![01](/assets/images/2022-05-29/25.jpg)
![01](/assets/images/2022-05-29/26.jpg)
![01](/assets/images/2022-05-29/27.jpg)
![01](/assets/images/2022-05-29/28.jpg)
![01](/assets/images/2022-05-29/29.jpg)

![01](/assets/images/2022-05-29/30.jpg)
![01](/assets/images/2022-05-29/31.jpg)
![01](/assets/images/2022-05-29/32.jpg)
![01](/assets/images/2022-05-29/33.jpg)
![01](/assets/images/2022-05-29/34.jpg)
![01](/assets/images/2022-05-29/35.jpg)
![01](/assets/images/2022-05-29/36.jpg)
![01](/assets/images/2022-05-29/37.jpg)
![01](/assets/images/2022-05-29/38.jpg)
![01](/assets/images/2022-05-29/39.jpg)

![01](/assets/images/2022-05-29/40.jpg)
![01](/assets/images/2022-05-29/41.jpg)
![01](/assets/images/2022-05-29/42.jpg)
![01](/assets/images/2022-05-29/43.jpg)
![01](/assets/images/2022-05-29/44.jpg)
![01](/assets/images/2022-05-29/45.jpg)
![01](/assets/images/2022-05-29/46.jpg)
![01](/assets/images/2022-05-29/47.jpg)
![01](/assets/images/2022-05-29/48.jpg)
![01](/assets/images/2022-05-29/49.jpg)
![01](/assets/images/2022-05-29/50.jpg)

