---
layout: post
title:  "#0031 - Radio CASo - SONDA"
date:   2024-02-24 11:46:10 -0300
categories: post
image: '/assets/images/2024-02-24/flyer.jpg'
---

# Radio CASo | SONDA

### Especial radiofónico de 24 H

[link para escuchar](https://centrodeartesonoro.cultura.gob.ar/info/radio-caso/)

En esta edición de Banquete, los sellos que conforman el proyecto Sonda tomarán el control de **Radio CASo** durante 24 horas para transmitir todo su catálogo.
Durante esta maratón, se escucharán álbumes de artistas de Rayo Seco (AR), Archivo Veintidós (CL), G.R.U.M.E.L. (AR) y TapeLoopLab (CO).


**Rayo Seco** es un sello y productora de gestión colectiva que produce y difunde arte sonoro y música de autxr e incursiona en estéticas contemporáneas y experimentales, propone prácticas culturales situadas y de gestión colaborativa desde Fiske Menuco (General Roca) / Río Negro / Patagonia Norte.


**Archivo Veintidós** nace en 2019 desde Santiago, Chile como un sello fonográfico digital con la intención de abrir espacios para nuevas y nuevos artistas experimentales de Latinoamérica, y que exploran las fronteras de lo acústicamente posible e imposible dentro de un fonograma. Proyecto gestionado por Colectiva 22bits.


**G.R.U.M.E.L**, acrónimo de Grupo de Músicas Experimentales Libres germinó como una situación de diálogo a-dentro de una lista de e-mail. Un primer grupo de personas para probar afinidades-intensidades. Pronto devino en organizar eventos, hacer que sucedan cosas, generar espacios donde encontrarnos-y-compartirnos. Pensamos que organizar es el arte de suscitar encuentros, no necesariamente entre lo mismo y lo mismo; sino entre lo mismo y lo otro. Hay acontecimiento político cuando lo mismo se encuentra con lo otro, cuando se transgreden fronteras. Organizar es poner en circulación, promover lo común entre diferentes, lo compartido entre singularidades. G.R.U.M.E.L. se trata de un depósito que siempre hay que actualizar mediante una nueva acción; renovarlo, refrescarlo y recrearlo.

**TapeLoopLab** es un proyecto de exploración y experimentación sonora con interfaces análogas, creación con cintas de casettes, luthería experimental y uso de tecnologías obsoletas.


### Transmisión: Sábado 24 de febrero | 00 a 24 H


<hr>
<br>
![01](/assets/images/2024-02-24/flyer.jpg)
