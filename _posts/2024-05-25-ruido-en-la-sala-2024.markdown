---
layout: post
title:  "#0035 - RUIDO EN LA SALA - PLANTA INCLAN"
date:   2024-05-25 13:01:10 -0300
categories: post
image: '/assets/images/2024-05-25/flyer.jpeg'
---

# Ruido en la Sala - Ciclo de Noise y Música experimental


RUIDO en la SALA es un ciclo de noise y música experimental producida en vivo, un espacio colaborativo y seguro en el cual trabajadorxs del sonido y la música puedan compartir sus pruebas, escucharse mutuamente e intercambiar públicos/escuchas. Desde una curaduría afectiva combinamos proyectos que se hallan en los intersticios, fracturas y fugas de categorías, que se cruzan en el modo de hacer, de preguntarse, de improvisar. Fomentamos cruces y experiencias sensibles entre colegas y experimentadorxs.

Produce G.R.U.M.E.L:
Uchi @udeuchi, Daniel Bruno @brunodeapellido, Claudia Brito Parra @claudiabrrrrr y Rapo @fran.juan.cisco

Sábado 25 de mayo- 20 hs
www.plantainclan.com

Tocan en esta fecha

▪️Nomateo @nomateo

▪️Marté + Daniel Bruno @o0o00oooooo @brunodeapellido

▪️Racha. @_rrracha

Visuales de Milagro De Catamarca. @mdc1600_

Imagen para flyer: Claudia Brito Parra @claudiabrrrrr

<hr>
<br>
![01](/assets/images/2024-05-25/flyer.jpeg)

<br>
![01](/assets/images/2024-05-25/01.png)

<br>
![01](/assets/images/2024-05-25/02.jpg)

<br>
![01](/assets/images/2024-05-25/03.jpeg)
