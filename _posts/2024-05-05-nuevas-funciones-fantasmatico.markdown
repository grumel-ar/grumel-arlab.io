---
layout: post
title:  "#0034 - NUEVAS FUNCIONES FANTASMÁTICO"
date:   2024-05-05 13:06:10 -0300
categories: post
image: '/assets/images/2024-05-05/1.jpg'
---

# ¡Nuevas funciones!🕯️  FANTASMÁTICO 🕯️

Fantasmático es una experiencia estenopeica que utiliza el diseño de una cámara oscura para crear un entorno único en los Salones de Danza del C. C. Pasaje Dardo Rocha. A través de orificios en las ventanas se traspasan imágenes directas de las calles circundantes. Imágenes espectrales del entorno operadas desde el interior de la sala. La dimensión sonora es creada mediante la ejecución performática de instrumentos acústicos no tradicionales. La escena es guiada por Anfitriona a través de la penumbra.

🗓️ DOMINGOS 5 y 12 DE MAYO 15h

Salones de danza de la Escuela Taller - Pasaje Dardo Rocha - Calle 50 e 6 y 7, La Plata

🎟️ Localidades disponibles en la boletería del pasaje

Entrada general $5000 Reservas al 221 6134798


Direccion Escénica Gonzalo Monzón @gonzmonzon

Luthería experimental y Diseño Sonoro Gabriel Drah @drahgabriel

Performers sonoros G.R.U.M.E.L: Macarena Aguilar Tau @maq.arena, Juan Francisco Raposeiras (Rapo) @fran.juan.cisco, Gabriel Drah

Performers Visuales Gonzalo Monzón, Nahuel Lahora @sismosan Juan de la Cruz @juan.delacruz.1806

Actriz Anfitriona Amelia Pena @ameliapena_actriz

Producción Nahuel Lahora

Asistente Ramiro Manaut @ram0n.ram0n


Agradecimientos Escuela Taller Municipal de Arte, Secretaría de Cultura Municipalidad de La Plata @laplata.ciudadcultural  @escuela_taller_lp

Fotos por Mareano Van Gelderen @marean0

Diseño por Pablo Rabe @rabepablo


FestiFreak 20 Años

<hr>
<br>
![01](/assets/images/2024-05-05/2.jpg)
<br>
![01](/assets/images/2024-05-05/3.jpg)
<br>
![01](/assets/images/2024-05-05/4.jpg)

#estenopeica #pinholephotography #camaraestenopeica #pinholecamera #estenopo
