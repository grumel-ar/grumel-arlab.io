---
layout: post
title:  "#0002 - Droneday"
date:   2021-05-29 21:46:10 -0300
categories: pos
image: '/assets/images/2021-05-29/01.jpg'
---

## Situación construida

### [Psicogeografía](https://es.wikipedia.org/wiki/Psicogeograf%C3%ADa){:target="_blank"} por la deriva en la ciudad de La Plata

Movimiento registrado con una TASCAM el 29/05 en conmemoración al
[Droneday](https://droneday.org/){:target="_blank"}

Un [_détournement_](https://es.wikipedia.org/wiki/D%C3%A9tournement){:target="_blank"} del caminar y la escucha cotidianas

* 10 y 59 a 13 y 59
* 13 y 59 a Plaza Moreno
* diag 73 (14 y 50) hasta 16 y 47
* 16 y 47 a 11 y 47
* 11 y 47 a 11 y 53
* 11 y 53 a 10 y 53
* 10 y 53 hasta [Demeter](https://www.instagram.com/demeter_cerveza/){:target="_blank"}

### audio

<audio controls>
<source src="/assets/audios/2021-05-29/drone_day.mp3" type="audio/ogg">
</audio>

### fotis

![01](/assets/images/2021-05-29/01.jpg)
![02](/assets/images/2021-05-29/02.jpg)
![03](/assets/images/2021-05-29/03.jpg)
![04](/assets/images/2021-05-29/04.jpg)
