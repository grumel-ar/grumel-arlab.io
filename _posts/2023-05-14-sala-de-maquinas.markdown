---
layout: post
title:  "#0020 - Ruido en la Sala #1"
date:   2023-05-14 20:27:10 -0300
categories: post
image: '/assets/images/2023-05-14/sala_de_maquinas_1.jpg'
---

## RUIDO EN LA SALA

### Un ciclo de NOISE – viajes ruidosos por paisajes y galaxias de fantasía

<hr>

_DOMINGO 14 de MAYO - 19hs en @demaquinassala (Lavalle 1145). Entrada $1700_

### Marté
Productor(a) compositor(a), artistas sonor(a) /visual y DJ argentin(a) no binarie. Produce ambient, música electrónica experimental y diseño sonoro. Explora la creación de atmósferas con texturas sintéticas, melodías de influencia pop, evocaciones rítmicas Latinas y el uso de la síntesis FM en su paleta sónica. Es tecladista de La Piba Berreta y trabaja en lanzamiento de su primer LP.
[https://o0o00oooooo.bandcamp.com/](https://o0o00oooooo.bandcamp.com/)

<br>

### Enjambre CristaL
Ritual y sonambulismo. Luciérnagas filosas (Sofía Zeta + Florconvenas) se encuentran en océanos nocturnos donde la diversidad de texturas, los tonos graves, las repeticiones fugaces, lo ríspido y frágil, despliegan paisajes oníricos que invitan a la escucha atenta y profunda.

<br>

### Javier Bustos
Artista, compositor, performer y docente residente en Bs.As. Explora poéticas de escucha y produce a partir del uso de nuevos medios y tecnologías precarias, operando entre la música experimental, la instalación y las artes escénico-multimediales.

<br>

### Hernan Hayet
Compositor, docente y multiinstrumentista, se dedica en particular al bajo electrónico. Trabaja en música y pasajes sonoros de producciones audiovisuales y edito 4 discos solistas. Desarrolla PERONOISE proyecto musical Performático y Nancy, proyecto grupal.

[https://hernanhayet.wordpress.com/](https://hernanhayet.wordpress.com/)

[https://hernanhayet1.bandcamp.com/releases](https://hernanhayet1.bandcamp.com/releases)

<br>
<hr>
<br>
![01](/assets/images/2023-05-14/sala_de_maquinas_1.jpg)


<br>
<hr>
<video controls>
  <source src="/assets/videos/2023-05-14/grumel_sala_de_maquinas.mov" type="video/mp4">
</video>
