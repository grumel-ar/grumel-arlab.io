---
layout: post
title:  "#0011 - Encuentro Movimiento + G.R.U.M.E.L"
date:   2022-07-01 10:29:33 -0300
categories: post
image: '/assets/images/2022-07-01/flyer.jpg'
---

# Encuentro Movimiento + G.R.U.M.E.L

![01](/assets/images/2022-07-01/flyer.jpg)<br>
<br>

💎💎💎💎💎💎💎💎💎💎💎💎El 1 de julio  te invitamos a acercarte a este espacio de encuentro e improvisación en danza.

💎Desde lo sonoro nos estarán acompañando les amores-amiges de G.R.U.M.E.L (Grupo de Musicas Experimentales Libre)
@lucavonr @pacoplastilina y @claudiabrrrrrr

💎La cita es en Vil Teatro El viernes 1 de julio de 15 a 17 h. y el valor sugerido de la propuesta es de $400

💎Tráete ropa cómoda, lo que necesites para estar cómode y una muda para cambiarte al salir.

💎 💎💎💎💎💎💎💎💎💎Venite a crear y disfrutar otras maneras de existir juntes🫂

💎Te esperamos!!
 Impulsan @copelloconi de @vilteatrovil y @claudiabrrrrrr de @marea.areademovimiento

#bailarconotrxs #bailarcomoquieras #jamdanzacontemporánea #grumel
#improvisación #culturaindependiente #autogestión #fisquemenuco #movimientocontemporáneo #laplata #vilteatro
