---
layout: post
title:  "#0021 - NANCY + MAQ + Costi Eliggi + Paulo Pecora + Make + Dj Pony"
date:   2023-06-10 12:27:10 -0300
categories: post
image: '/assets/images/2023-06-10/grumel_nancy.jpg'
---

# NANCY + MAQ + Costi Eliggi + Paulo Pecora + Make + Dj Pony

Grumel acompaña este evento. 10 de junio

El sábado 10 de junio se viene esta intensa maratona de festejo y celebración porque se inaugura una casa pero también porque nos gusta el agite y la autogestión y bueno, a fin de cuentas también un poco porque sí

Un rejunte de lujo que arranca con las mágicas canciones de @costi.eliggi, sigue con una secuencia cautivadora de @maq.arena, GRUMEL y @diegomakedonsky haciéndonos volar con universos sonoros 🎧 y nos cautiva con la irrepetible primera vez de les querides @somosnancy en suelo platense.
De paso, flotaremos entre unas proyecciones hechiceras traídas directamente de la galaxia @papecora 📽 y todo el vuelo sobrenatural de @poni. para hacernos bailar hasta que nos echen

Se entra GRATIS y habrá comidas y bebidas para mantenernos nutrides e hidratades, pero también para apoyar a esta banda prodigiosa que nos regala una noche de felicidad 🍽

La cosa será al aire libre, así que importante saber que si llueve ☔ REPROGRAMAREMOS! Habrá algún fueguito pero igual no se olviden de traer abriguitos

Dire por privado, compartan, inviten y vengan!

<hr>

<br>
![01](/assets/images/2023-06-10/grumel_nancy.jpg)
