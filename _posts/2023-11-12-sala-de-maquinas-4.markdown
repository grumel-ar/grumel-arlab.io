---
layout: post
title:  "#0028 - Ruido en la Sala #4"
date:   2023-11-12 15:27:10 -0300
categories: post
image: '/assets/images/2023-11-12/flyer.jpg'
---

## RUIDO EN LA SALA #4

### Un ciclo de NOISE – viajes ruidosos por paisajes y galaxias de fantasía


DOMINGO 12/11 19hs en @demaquinassala (Lavalle 1145)

♒︎ Entrada $2000 anticipada y $3000 en puerta

— FRAN PROYEKT—
Proyecto de experimentación sonora y visual, oscilaciones entre electrónica, canciones de amor, noise y loops que desembocan en un universo propio con un trip espeso y simple.  En FRAN PROYEKT dialogan la estética Trash, Kitsch, como crítica lúdica a las formas tradicionales del arte culto. Trans-mutaciones sonoras, atmósferas intimistas y ambientales.

www.linktr.ee/franfranf

 @franproyekt


— PABLO DÍAZ Y RVRLS—
Pablo Díaz es músico, improvisador y artista sonoro de Buenos Aires. Baterista que busca trascender estereotipos y profundizar en la experiencia sonora. Indaga en el ritmo en otros términos temporales al que le fue asignado por convención. Su trabajo hace eje en el sonido como materia, la creación de obra (conciertos, performances e instalaciones) y  la propuesta de experiencias.

www.pablodiaz.com.ar

@p.ablod.iaz


rvrls es un proyecto rural, de campo abierto coordinado por Braulio Urbano, artista sonoro y audiovisual oriundx de La Pampa. Su trabajo se centra en el campo del diseño y paisaje sonoro, a través de herramientas de la electrónica analógica y digital, instrumentos electroacústicos y grabaciones de campo. La improvisación, el registro y la manipulación sonora en medios obsoletos son parte de sus investigaciones e incursiones actuales.

https://rvrls.tumblr.com/

@_rvrls


— ALGO—
Proyecto de improvisación libre enmarcada en los límites materiales que presentan las herramientas sonoras y el espacio físico. Sin límites no hay algo. Alter ego de Ju Li, creado especialmente para este ciclo. Una ensalada de noise, ambient y delirio.

https://soundcloud.com/jjjuuullliii

@hacer_algo


Producción : @udeuchi @brunodeapellido @fran.juan.cisco  y @claudiabrrrrrr  G.R.U.M.E.L
Diseño Flyer : @claudiabrrrrrr
Acompañan, apañan y fomentan @cristiancarracedolauretti @claudiabrrrrrr @joaquinsegade


<br>
<hr>
<br>
![01](/assets/images/2023-11-12/flyer.jpg)
