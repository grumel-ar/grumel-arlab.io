---
layout: post
title:  "#0010 - G.R.U.M.E.L Rayoseco #NUMERAL en C.A.O.S"
date:   2022-06-26 01:46:10 -0300
categories: post
image: '/assets/images/2022-06-26/flyer.jpg'
---

# G.R.U.M.E.L + Rayoseco + #NUMERAL | C.A.O.S

![01](/assets/images/2022-06-26/flyer.jpg)<br>
<br>

para este domingo en C.A.O.S ~Club de Artes y Ocios Sonoros~ @clubdeartesyocios
organizamos otra fecha de G.R.U.M.E.L junto a #NUMERAL @neue.numeral
y al sello patagonico Rayo Seco Discos @rayosecodiscos

estamos:

-> [Una Masa] @unamasalp

-> [FELINUS] @rocio_segura_irurueta desde MDQ

-> [Leo Mudo] @mudoleo

-> [Flor con Venas + Jacoba Inucase] @florconvenas (CABA) @claudiabrrrrrr desde Fiske Menuko

<br>
𝐅𝐋𝐘𝐄𝐑 𝐩𝐨𝐫 𝐑𝐞𝐠𝐢𝐧𝐚 𝐑𝐚𝐟𝐟𝐞𝐭𝐭𝐨
<br>
[https://neuenumeral.com](https://neuenumeral.com)
