---
layout: post
title:  "#0015 - G.R.U.M.E.L + #NUMERAL 5"
date:   2022-08-21 22:15:00 -0300
categories: post
image: '/assets/images/2022-08-21/flyer.jpg'
---

# G.R.U.M.E.L + #NUMERAL 5

![01](/assets/images/2022-08-21/flyer.jpg)<br>
<br>

El domingo que viene volvemos a nuestro amado @vilteatrovil para una fecha más (la quinta!) de la conjunción astral entre #grumel y @neue.numeral

A saber:
El dúo de baterías de @seba_piatti y @luqui.munoz . Ritmo como textura.

La anticipada propuesta electrónica de @oreopanax .

Y el “solo bien se lame” set de @jorge_espinal , que vuelve a La Plata después de un largo tiempo.

Los esperamos desde las 19 con barra y las exquisiteses de @martinmaggio_ .

A partir del miércoles, anticipadas en @la_disqueria .

Y como siempre, gracias @estachica_regina por el arte para difusión.


![01](/assets/images/2022-08-21/1.jpg)
![01](/assets/images/2022-08-21/2.jpg)
![01](/assets/images/2022-08-21/3.jpg)
![01](/assets/images/2022-08-21/4.jpg)
![01](/assets/images/2022-08-21/5.jpg)
![01](/assets/images/2022-08-21/6.jpg)
![01](/assets/images/2022-08-21/7.jpg)
![01](/assets/images/2022-08-21/8.jpg)
![01](/assets/images/2022-08-21/9.jpg)
![01](/assets/images/2022-08-21/10.jpg)
![01](/assets/images/2022-08-21/11.jpg)
![01](/assets/images/2022-08-21/12.jpg)
![01](/assets/images/2022-08-21/13.jpg)
![01](/assets/images/2022-08-21/14.jpg)
![01](/assets/images/2022-08-21/15.jpg)
![01](/assets/images/2022-08-21/16.jpg)
![01](/assets/images/2022-08-21/17.jpg)
![01](/assets/images/2022-08-21/18.jpg)
![01](/assets/images/2022-08-21/19.jpg)
![01](/assets/images/2022-08-21/20.jpg)
![01](/assets/images/2022-08-21/21.jpg)
![01](/assets/images/2022-08-21/22.jpg)
![01](/assets/images/2022-08-21/23.jpg)
![01](/assets/images/2022-08-21/24.jpg)
![01](/assets/images/2022-08-21/25.jpg)
![01](/assets/images/2022-08-21/26.jpg)
![01](/assets/images/2022-08-21/27.jpg)
![01](/assets/images/2022-08-21/28.jpg)
![01](/assets/images/2022-08-21/29.jpg)
![01](/assets/images/2022-08-21/30.jpg)
![01](/assets/images/2022-08-21/31.jpg)
