---
layout: post
title:  "#0005 - G.R.U.M.E.L + NUMERAL vol 1"
date:   2021-12-05 02:46:10 -0300
categories: post
image: '/assets/images/2021-12-05/flyer.jpg'
---

# VIL Teatro | GRUMEL en colaboración con #NUMERAL

![01](/assets/images/2021-12-05/flyer.jpg)<br>

<br>
𝘿𝙊𝙈𝙄𝙉𝙂𝙊 5 𝘿𝙀 𝘿𝙄𝘾𝙄𝙀𝙈𝘽𝙍𝙀

【ＰＲＯＹＥＣＴＯ　ＥＸＰＬＯＲＡ】 macarena aguilar tau y camila aguilar

** 𝙘𝙚𝙡𝙡𝙤, 𝙘𝙡𝙖𝙧𝙞𝙣𝙚𝙩𝙚 𝙮 𝙚𝙡𝙚𝙘𝙩𝙧ó𝙣𝙞𝙘𝙖 **

<br>
<br>
【ＰＲＯＳＣＩＡ　／　ＦＯＲＭＩＣＡ　／　ＣＡＲＲＡＣＥＤＯ　ＬＡＵＲＥＴＴＩ】

** 𝙨𝙖𝙭𝙤𝙨 𝙮 𝙚𝙡𝙚𝙘𝙩𝙧ó𝙣𝙞𝙘𝙖 **

<br>
<br>
【ＭＵＲＫＩ　/　ＲＡＰＯ】

** 𝙨𝙖𝙢𝙥𝙡𝙖𝙙𝙚𝙡𝙞𝙖 **

<br>
<br>

(っ◔◡◔)っ
<br>
𝐅𝐋𝐘𝐄𝐑 𝐩𝐨𝐫 𝐑𝐞𝐠𝐢𝐧𝐚 𝐑𝐚𝐟𝐟𝐞𝐭𝐭𝐨
<br>
[https://neuenumeral.com](https://neuenumeral.com)

<hr>
Fotos por [Felicitas PH](https://www.instagram.com/felisisti.ph/)

![01](/assets/images/2021-12-05/5.jpg)
![01](/assets/images/2021-12-05/6.jpg)
![01](/assets/images/2021-12-05/7.jpg)
![01](/assets/images/2021-12-05/8.jpg)
![01](/assets/images/2021-12-05/12.jpg)
![01](/assets/images/2021-12-05/24.jpg)
![01](/assets/images/2021-12-05/64.jpg)
![01](/assets/images/2021-12-05/65.jpg)
![01](/assets/images/2021-12-05/76.jpg)
![01](/assets/images/2021-12-05/77.jpg)
![01](/assets/images/2021-12-05/78.jpg)
![01](/assets/images/2021-12-05/116.jpg)
![01](/assets/images/2021-12-05/123.jpg)
![01](/assets/images/2021-12-05/127.jpg)
![01](/assets/images/2021-12-05/129.jpg)
![01](/assets/images/2021-12-05/159.jpg)
![01](/assets/images/2021-12-05/161.jpg)
![01](/assets/images/2021-12-05/162.jpg)
![01](/assets/images/2021-12-05/168.jpg)
![01](/assets/images/2021-12-05/173.jpg)
![01](/assets/images/2021-12-05/181.jpg)
![01](/assets/images/2021-12-05/186.jpg)
![01](/assets/images/2021-12-05/195.jpg)
![01](/assets/images/2021-12-05/237.jpg)
