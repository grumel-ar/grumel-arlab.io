---
layout: post
title:  "#0026 - FESTI FREAK  "
date:   2023-10-7 12:27:10 -0300
categories: post
image: '/assets/images/2023-10-07/ff1.jpg'
---

## FESTI FREAK | MUSICALIZADAS

Celebramos una nueva edición del festival con dos propuestas de musicalización que vuelven sobre el rito contemplativo de las imágenes en movimiento. Dos shows acústicos que dialogan con fantasmas y nos convidan una mirada encantada del mundo. EL DESAMBLE, con su precisa ejecución, nos saca a bailar sobre las más variadas invenciones de pioneros y vanguardias cinematográficas. FANTASMATICO se presenta como una performance de ruidos y penumbras, donde los dispositivos más simples de la proyección visual son activados para traspasar imágenes directas de las calles circundantes al interior de la sala.


FANTASMÁTICO

🗓 Sábado 7 de Octubre 16hs

🗓 Domingo 8 de Octubre 16hs

Salones de Danza de la Escuela Taller, Pasaje Dardo Rocha, calle 50 e/ 6 y 7, La Plata
Valor de la entrada: $1000

Instalación audiovisual que utiliza el diseño de una cámara oscura para crear un entorno único. A través de orificios en las ventanas se traspasan imágenes directas de las calles circundantes que son proyectadas sobre paños traslúcidos operados desde el interior de la sala. Imágenes espectrales del entorno crean una escena cautivadora acompañada de la ejecución performática de instrumentos acústicos no tradicionales. La escena es habitada por Anfitriona, personaje lúdico que guía a les visitantes a través de la penumbra.

Diseño de Espacio Escénico Gonzalo Monzón @gonzmonzon

Luthería experimental y Diseño Sonoro Gabriel Drah @drahgabriel

Diseño de pantallas Fran Carranza @carranzafranfran

Performers sonoros G.R.U.M.E.L: Macarena Aguilar Tau @maq.arena, Juan Francisco Raposeiras (Rapo) @fran.juan.cisco, Gabriel Drah

Performers Visuales Gonzalo Monzón, Fran Carranza, Nahuel Lahora

Actriz Anfitriona Amelia Pena @ameliapena_actriz

Producción Nahuel Lahora @sismosan

Asistente Juan de la Cruz @juan.delacruz.1806

Agradecimientos Escuela Taller Municipal de Arte - Dir. Sergio Casanovas, Franco Cerana, Franco Palazzo, Programa de Becas PAR UNLP

<br>
![01](/assets/images/2023-10-07/ff2.jpg)

<br>
![01](/assets/images/2023-10-07/ff3.jpg)
