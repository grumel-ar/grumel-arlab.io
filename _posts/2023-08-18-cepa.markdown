---
layout: post
title:  "#0024 - C.E.P.A: Conciertos Experimentales por la Patagonia Argentina"
date:   2023-08-18 20:10:32 -0300
categories: post
image: '/assets/images/2023-08-18/cepa.jpg'
---

## C.E.P.A: Conciertos Experimentales por la Patagonia Argentina

nos gusta hacer acrónimos: C.E.P.A Conciertos Experimentales por la Patagonia Argentina

Presentamos un recorrido sureño-montañoso a través de varias localidades:
18/08 Lago Puelo @lateta.arteculinarioycultural
19/08 Epuyen @punto40epuyen2
20/08 El Bolsón @espacioavefenix
25/08 Esquel @planb_esquel

Llevo cosas de G.R.U.M.E.L, voy con mi moniker Rapo

a hacer musicas en la comarca con personas @who7io @melodrumma  @antenayanum @maiakoenig @ignaciocantumass @montanasonora @luz.delviento @nbiexperience @ramacerratto @un_viajecito_lulu @tropicalponcho @lapista.escenicas #GRUMEL

flyer @melodrumma

<hr>
<br>
![01](/assets/images/2023-08-18/cepa.jpg)
