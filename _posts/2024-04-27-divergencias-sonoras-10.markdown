---
layout: post
title:  "#0033 - Divergencias Sonoras #10"
date:   2024-04-27 13:10:00 -0300
categories: post
image: '/assets/images/2024-04-27/flyer.jpg'
---

¡Gracias Divergencias Sonoras por la invitación! En ésta, la décima edición de su recomendable ciclo, participamos con G.R.U.M.E.L en la curaduría de dos actos.

@inaquiesque
@arianozeta & @n.nnemo

@ciclo_bauhaus @cicloespecie y el propio Divergencias, por supuesto también se hacen presentes en el line up:

@jung.li_
@sofiazetaediciones
@julian.di.pietro
@leocullari
@mlau_moran
@martinremiro

@otracasa_ será quien albergue esta encuentro, el sábado 27 de abril a las 20:30 horas.

Entradas anticipadas por DM a @martinremiro @recabarrennat (Divergencias Sonoras)

Flyer @iriojuan

Nos vemos allí!

<hr>
<br>
![01](/assets/images/2024-04-27/flyer.jpg)
