---
layout: post
title:  "#0025 - Ruido en la Sala #3"
date:   2023-09-10 18:27:10 -0300
categories: post
image: '/assets/images/2023-09-10/flyer.jpg'
---

## RUIDO EN LA SALA #3

### Un ciclo de NOISE – viajes ruidosos por paisajes y galaxias de fantasía


◌● DOMINGO 10 de Septiembre 19hs en @demaquinassala (Lavalle 1145)

♒︎ [Entrada $2000 anticipada](https://publico.alternativateatral.com/entradas85541-ruido-en-la-sala-iii?o=14)

$2500 en puerta
<hr>

— Valeria V—
Valeria V es el proyecto de Valeria Vinograd, plantea un acercamiento al sonido mediante herramientas electrónicas, usa samplers y sintetizadores analógicos. Con la exploración sonora como principal interés construye ritmos complejos, a veces primitivos. @valeriavai
<hr>

— UCHI—
Uchi es Improvisadora Sonora y participa en proyectos como Asistente técnica y artística. Actualmente investiga las posibles resultantes del procesamiento de acoples.
@udeuchi
<hr>

— Valentín Pelisch—
A Irina Nistor (A/V live-set)
x Valentín Pelisch
Compositor, artista de foley y editor audiovisual. Su producción incluye obras para ensamble, performances, instalaciones y video. Junto a Pablo  Boltshauser conforma BASURA,  un  dúo experimental que trabaja sobre archivos  rescatados  de  internet. Es co-programador  del  ciclo  de  conciertos de música contemporánea y experimental Mínimo un Lunes.  http://www.valentinpelisch.com.ar
 @vavlvevnvtvivnv_peplpipspcphp
<hr>

— Rapo + Chris + Murki—
C.R.M (acrónimo de Chris, Rapo y Murki) es una (de)formación que surgió a partir de encuentros nómades de exploración sonora entre tres ciudades. Es una conexión modular de seres recorriendo pulsos, ritmos e intensidades. También es una reflexión instintiva sobre un lenguaje-escucha común que se materializa provisoriamente para ser re-planteada y re-sampleada en cada sesión de improvisación. Todo bajo una capa de distorsión visual a cargo de Surkan.
 @fran.juan.cisco @cristiancarrac @sri_demaratus @surkan_
<hr>

Producción : Claudia Brito @claudiabrrrrr
G.R.U.M.E.L
Diseño Flyer : @claudiabrrrrr
Acompañan, apañan y fomentan @fran.juan.cisco @cristian @clau @joa

#arteindependienteyautogestivo #producciónenvivo  #noise #musicaexperimental #artesonoro #GRUMEL #salademáquinas





<br>
<hr>
<br>
![01](/assets/images/2023-09-10/flyer.jpg)
