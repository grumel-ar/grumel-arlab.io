---
layout: post
title:  "#0027 - SONDA  "
date:   2023-10-20 11:27:10 -0300
categories: post
image: '/assets/images/2023-10-20/s1.png'
---

## SONDA

SONDA, el proyecto de colaboración que reúne a diversos sellos latinoamericanos
de música experimental, comienza su travesía.

Ocho artistas sonoros y sonoras de Latinoamérica colaborando de manera intensiva y
explorando las fronteras de la música experimental. Esa es la premisa inicial de la que
parte SONDA, un proyecto internacional en el que cuatro sellos y colectivos sonoros
se unen para buscar nuevos cruces creativos.
SONDA es una propuesta de creación colaborativa intensiva de arte sonoro y
experimental impulsada por Rayo Seco (AR) y Enjambre (AR), en colaboración con
TapeLoopLab (CO), Archivo Veintidós (CL) y G.R.U.M.E.L. (AR) y apoyada por
Ibermúsicas.

Partiendo de un repositorio sonoro común, cuatro sondas creativas se embarcan en
un laboratorio de colaboración. El repositorio contiene ondas con materiales diversos:
improvisaciones, registros sonoros, voces, testimonios, soundscapes, archivos, bases,
found footage, samplers... A partir de ahí, SONDA propone un viaje de exploración de
tres semanas de reflexión y co-creación entre artistas, un tiempo para intervenir,
modificar, reeditar, copipastiar y reinventar. El formato se cierra con la participación
de cuatro artistas visuales de distintos puntos del continente que sumarán sus
imágenes al universo sonoro.

<br>
![01](/assets/images/2023-10-20/s2.png)

<br>
![01](/assets/images/2023-10-20/s3.png)
