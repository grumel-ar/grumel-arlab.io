---
layout: post
title:  "#0008 - G.R.U.M.E.L + NUMERAL vol 3"
date:   2022-04-24 02:46:10 -0300
categories: post
image: '/assets/images/2022-04-24/flyer.jpg'
---

# VIL Teatro | GRUMEL en colaboración con #NUMERAL vol 3

![01](/assets/images/2022-04-24/flyer.jpg)<br>
<br>

Tercera edición G.R.U.M.E.L Y #NUMERAL

Gabriel Cichero

Amateur, Gabriela Areal + Daniel Bruno

Gabriel Drah + Fraxu + Victoria Parada + Pamela Esquivel

<br>
𝐅𝐋𝐘𝐄𝐑 𝐩𝐨𝐫 𝐑𝐞𝐠𝐢𝐧𝐚 𝐑𝐚𝐟𝐟𝐞𝐭𝐭𝐨
<br>
[https://neuenumeral.com](https://neuenumeral.com)

<hr>
Fotos por [Felicitas PH](https://www.instagram.com/felisisti.ph/)

![01](/assets/images/2022-04-24/1.jpg)
![01](/assets/images/2022-04-24/2.jpg)
![01](/assets/images/2022-04-24/3.jpg)
![01](/assets/images/2022-04-24/4.jpg)
![01](/assets/images/2022-04-24/5.jpg)
![01](/assets/images/2022-04-24/6.jpg)
![01](/assets/images/2022-04-24/7.jpg)
![01](/assets/images/2022-04-24/8.jpg)
![01](/assets/images/2022-04-24/9.jpg)
![01](/assets/images/2022-04-24/10.jpg)
![01](/assets/images/2022-04-24/11.jpg)
![01](/assets/images/2022-04-24/12.jpg)
![01](/assets/images/2022-04-24/13.jpg)
![01](/assets/images/2022-04-24/14.jpg)
![01](/assets/images/2022-04-24/15.jpg)
![01](/assets/images/2022-04-24/16.jpg)
![01](/assets/images/2022-04-24/17.jpg)
![01](/assets/images/2022-04-24/18.jpg)
![01](/assets/images/2022-04-24/19.jpg)
![01](/assets/images/2022-04-24/20.jpg)
![01](/assets/images/2022-04-24/21.jpg)
![01](/assets/images/2022-04-24/22.jpg)
![01](/assets/images/2022-04-24/23.jpg)
![01](/assets/images/2022-04-24/24.jpg)
![01](/assets/images/2022-04-24/25.jpg)
![01](/assets/images/2022-04-24/26.jpg)
![01](/assets/images/2022-04-24/27.jpg)
![01](/assets/images/2022-04-24/28.jpg)
![01](/assets/images/2022-04-24/29.jpg)
![01](/assets/images/2022-04-24/30.jpg)
![01](/assets/images/2022-04-24/31.jpg)
![01](/assets/images/2022-04-24/32.jpg)
![01](/assets/images/2022-04-24/33.jpg)
![01](/assets/images/2022-04-24/34.jpg)
![01](/assets/images/2022-04-24/35.jpg)
![01](/assets/images/2022-04-24/36.jpg)
![01](/assets/images/2022-04-24/37.jpg)
![01](/assets/images/2022-04-24/38.jpg)
