---
layout: post
title:  "#0006 - Improvisación audiovisual VIL Teatro"
date:   2022-02-23 02:46:10 -0300
categories: post
image: '/assets/images/2022-02-23/flayer.jpg'
---

# Improvisación audiovisual en VIL Teatro

![01](/assets/images/2022-02-23/flayer.jpg)

Cuando viene Clau Brito a La Plata hacemos cosas musicales.
Esta vuelta nos organizamos a improvisar.

2 dias ensayo en la casa de Rapo, al 3er dia abrir-mostrar en VIL.

El grupo de danza-entrenamiento de Delfi Serra, Calientes y Frescas, también participa


* RAPO
* CLAU BRITO
* GABRIEL DRAH
* MAX JERINGA
* PACO PLASTILIN
* RO VONR
* Calientes y Frescas, grupo de danza-entrenamiento coordinado por Delfina Serra
* quien quiera <3


Videos por @zenciliaming

<video controls>
  <source src="/assets/videos/2022-02-23/01.mp4" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2022-02-23/02.mp4" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2022-02-23/03.mp4" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2022-02-23/04.mp4" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2022-02-23/05.mp4" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2022-02-23/06.mp4" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2022-02-23/07.mp4" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2022-02-23/08.mp4" type="video/mp4">
</video>
