---
layout: post
title:  "#0012 - Presentación de Materiales by Maq"
date:   2022-07-24 10:29:33 -0300
categories: post
image: '/assets/images/2022-08-24/flayer.jpg'
---

# Presentación de Materiales by Maq

![01](/assets/images/2022-08-24/flayer.jpg)<br>
<br>

Este domingo 24/7 acontece un nuevo GRUMEL desde las 18hs

La presentación del disco de @maq.arena: Materiales, junto a @fraxufraxu

@matt_cianfo + radio GRUMEL a cargo de @pacoplastilina + invitadx sorpresa

flyer @pacoplastilina

#GRUMEL

<br>
[https://maq-arena.bandcamp.com/album/materiales](https://maq-arena.bandcamp.com/album/materiales)
<br>

<p>
<video controls loop>
  <source src="/assets/images/2022-08-24/videoflyer.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
</p>

![01](/assets/images/2022-08-24/1.jpg)
![01](/assets/images/2022-08-24/2.jpg)
![01](/assets/images/2022-08-24/3.jpg)
![01](/assets/images/2022-08-24/4.jpg)
![01](/assets/images/2022-08-24/5.jpg)
![01](/assets/images/2022-08-24/6.jpg)
![01](/assets/images/2022-08-24/7.jpg)
![01](/assets/images/2022-08-24/8.jpg)
![01](/assets/images/2022-08-24/9.jpg)
![01](/assets/images/2022-08-24/10.jpg)
![01](/assets/images/2022-08-24/11.jpg)
![01](/assets/images/2022-08-24/12.jpg)
![01](/assets/images/2022-08-24/13.jpg)
![01](/assets/images/2022-08-24/14.jpg)
![01](/assets/images/2022-08-24/15.jpg)
![01](/assets/images/2022-08-24/16.jpg)
![01](/assets/images/2022-08-24/17.jpg)
![01](/assets/images/2022-08-24/18.jpg)
![01](/assets/images/2022-08-24/19.jpg)
![01](/assets/images/2022-08-24/20.jpg)
![01](/assets/images/2022-08-24/21.jpg)
![01](/assets/images/2022-08-24/22.jpg)
![01](/assets/images/2022-08-24/23.jpg)
![01](/assets/images/2022-08-24/24.jpg)
![01](/assets/images/2022-08-24/25.jpg)
![01](/assets/images/2022-08-24/26.jpg)
![01](/assets/images/2022-08-24/27.jpg)
![01](/assets/images/2022-08-24/28.jpg)
![01](/assets/images/2022-08-24/29.jpg)
![01](/assets/images/2022-08-24/30.jpg)
![01](/assets/images/2022-08-24/31.jpg)
![01](/assets/images/2022-08-24/32.jpg)
![01](/assets/images/2022-08-24/33.jpg)
![01](/assets/images/2022-08-24/34.jpg)
![01](/assets/images/2022-08-24/35.jpg)
![01](/assets/images/2022-08-24/36.jpg)
![01](/assets/images/2022-08-24/37.jpg)
![01](/assets/images/2022-08-24/38.jpg)
![01](/assets/images/2022-08-24/39.jpg)
![01](/assets/images/2022-08-24/40.jpg)
![01](/assets/images/2022-08-24/41.jpg)
![01](/assets/images/2022-08-24/42.jpg)
![01](/assets/images/2022-08-24/43.jpg)
![01](/assets/images/2022-08-24/44.jpg)
