---
layout: post
title:  "#0022 - Ruido en la Sala #2"
date:   2023-07-16 20:27:10 -0300
categories: post
image: '/assets/images/2023-07-16/flyer.jpg'
---

## RUIDO EN LA SALA #2

### Un ciclo de NOISE – viajes ruidosos por paisajes y galaxias de fantasía


◌● DOMINGO 16 de JULIO - 19hs en @demaquinassala (Lavalle 1145)

<hr>

— SYMI —

Proyecto de Fernanda Flores, ambientes emocionales, mediante la síntesis sonora, concreta y electrónica busca plasmar ambientes emocionales. @symi___
<hr>

— Joaquina con GRACIA —

Joaquina es un nuevo heterónimo de Joaquín Segade: músico, compositor y experimentador, que siempre está buscando el modo de tocar por primera vez.
En esta ocasión, atraído por el ruido, la sala, el misterio y la oportunidad de tocar con Gracia.
@joaquinsegade
GRACIA es un proyecto solista de Gracia Fernández que busca a través de la
improvisación, un lenguaje donde construir un paisaje sonoro-amorfo. Voces y susurros, una guitarra, sonidos y silencios recopilados para componer una poesía dispersa. @laasaji
<hr>

— MAQ —

Macarena Aguilar Tau aka, artista sonora nacida en la ciudad de La Plata. Comparte propuestas sonoras experimentales que inician en la escucha, grabación y procesamiento. Sus presentaciones en vivo proponen sesiones improvisatorias de “sampling”, una amplia paleta sonora de registros realizados con instrumentos acústicos, foleys y sintetizadores. En escena entreteje sonidos y construye collages sonoros que experimentan con las temporalidades, polirritmias, afinaciones y texturas. @maq.arena
<hr>

— Guillermo Novelly + NVK —

Guillermo es Músico, artista sonoro y docente. Investiga los posibles cruces entre la electrónica
experimental, el proceso en tiempo real de objetos e instrumentos DIY y convencionales, y la libre improvisación. Es artista residente y organizador junto a Nicolás Vázquez_NKV del ciclo de música
experimental y arte sonoro Diálogos con sede en Buenos Aires.  @guillermo_novelli    @ciclo_dialogos
Linktr.ee/GuillermoNovelli
Nicolás lleva adelante NKV, un proyecto músical experimental y fusión electrónica que promueve la movilidad, experimentación de sentidos e intercambio artístico colaborativo con otros proyectos. Un universo sonoro de tecnologías analógicas y maquinas digitales e intervenciones, tienen la intención de desplazar los márgenes de las practicas artísticas.
https://linktr.ee/nico_vzqz    @nick_vzqz
<hr>


Producción : Claudia Brito @claudiabrrrrr

G.R.U.M.E.L

Diseño Flyer : @claudiabrrrrre

Acompañan, apañan y fomentan @rapo @clau @joa


#arteindependienteyautogestivo #producciónenvivo  #noise #musicaexperimental #artesonoro #GRUMEL #salademáquinas


<br>
<hr>
<br>
![01](/assets/images/2023-07-16/flyer.jpg)
