---
layout: post
title:  "#0007 - G.R.U.M.E.L + NUMERAL vol 2"
date:   2022-03-13 02:46:10 -0300
categories: post
image: '/assets/images/2022-03-13/feed.jpg'
---

# En Eso Estamos | GRUMEL en colaboración con #NUMERAL vol 2

![01](/assets/images/2022-03-13/feed.jpg)<br>
<br>

Segunda edición G.R.U.M.E.L Y #NUMERAL

macarena aguilar tau & rocío von reichenbach

inti pujol (MZA) & cristian carracedo lauretti

jlost (MZA) & solana lanchares vidart (LP)

<br>
𝐅𝐋𝐘𝐄𝐑 𝐩𝐨𝐫 𝐑𝐞𝐠𝐢𝐧𝐚 𝐑𝐚𝐟𝐟𝐞𝐭𝐭𝐨
<br>
[https://neuenumeral.com](https://neuenumeral.com)
