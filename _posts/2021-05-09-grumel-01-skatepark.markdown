---
layout: post
title:  "#0001 - Skatepark"
date:   2021-05-09 20:27:10 -0300
categories: post
---

Skatepark: _(contra)acción-perfomatica-ruidosa_

<iframe width="560" height="315" src="https://www.youtube.com/embed/xa5EQazsvXg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### fotis

![01](/assets/images/2021-05-09/01.jpg)
![02](/assets/images/2021-05-09/02.jpg)
![03](/assets/images/2021-05-09/03.jpg)
![04](/assets/images/2021-05-09/04.jpg)
![05](/assets/images/2021-05-09/05.jpg)
![06](/assets/images/2021-05-09/06.jpg)
![07](/assets/images/2021-05-09/07.jpg)
![08](/assets/images/2021-05-09/08.jpg)
![09](/assets/images/2021-05-09/09.jpg)
![10](/assets/images/2021-05-09/10.jpg)
![10](/assets/images/2021-05-09/11.jpg)
![10](/assets/images/2021-05-09/12.jpg)
![10](/assets/images/2021-05-09/13.jpg)


### videos

<video controls>
  <source src="/assets/videos/2021-05-09/01.mp4" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2021-05-09/02.mp4" type="video/mp4">
</video>

<video controls>
  <source src="/assets/videos/2021-05-09/04.mp4" type="video/mp4">
</video>


### audios

<audio controls>
  <source src="/assets/audios/2021-05-09/01.mp3" type="audio/ogg">
</audio>

<audio controls>
  <source src="/assets/audios/2021-05-09/02.mp3" type="audio/ogg">
</audio>

<audio controls>
  <source src="/assets/audios/2021-05-09/03.mp3" type="audio/ogg">
</audio>

<audio controls>
  <source src="/assets/audios/2021-05-09/04.mp3" type="audio/ogg">
</audio>
