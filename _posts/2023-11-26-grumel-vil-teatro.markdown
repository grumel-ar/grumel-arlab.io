---
layout: post
title:  "#0029 - G.R.U.M.E.L VIL TEATRO"
date:   2023-11-26 09:46:10 -0300
categories: post
image: '/assets/images/2023-11-26/flyer.jpg'
---

# G.R.U.M.E.L | VIL Teatro

Este domingo 26/11 desde las 20hs en VIL Teatro nos encontramos para hacer otro G.R.U.M.E.L

* dúo ad-hocrático de Pablo Elinbaum (guitarra preparada) y Seba Piatti (percusión)
* Barullo (Martin Remiro y Recabarrennat)
* Pablo Verón y Aldobnz con su dúo @ouuuuuuud

Cocina Martin Maggio

Luces Suga


<hr>
<br>
![01](/assets/images/2023-11-26/flyer.jpg)
