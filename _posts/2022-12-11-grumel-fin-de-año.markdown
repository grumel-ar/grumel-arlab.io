---
layout: post
title:  "#0019 - G.R.U.M.E.L fin de año"
date:   2022-12-11 09:15:00 -0300
categories: post
image: '/assets/images/2022-12-11/flyer.jpg'
---

# G.R.U.M.E.L fin de año

![01](/assets/images/2022-12-11/flyer.jpg)<br>
<br>

Cerramos el año G.R.U.M.E.L con esta celebración:

* surkan

* zencilia + vj code

* fraxu band

En VIL Teatro, nuestra casa

Flyer by @pacoplastilina
