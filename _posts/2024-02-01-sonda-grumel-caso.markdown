---
layout: post
title:  "#0030 - SONDA G.R.U.M.E.L en el CASo"
date:   2024-02-01 10:46:10 -0300
categories: post
image: '/assets/images/2024-02-01/flyer.jpg'
---

⁣El jueves 1° de febrero lxs invitamos a la inauguración de una instalación del proyecto de creación colectiva @sonda.cc en CAS0 @centrodeartesonoro

𝗦𝗢𝗡𝗗𝗔 es un proyecto que combina distintas expresiones sonoras y visuales. Se trata de una creación colectiva entre sellos latinoamericanos gestionada por @rayosecodiscos y @enjambrecc, donde participaron @rayosecodiscos #G.R.U.M.E.L., @tapelooplab (CO) y @archivo_veintidos (CL). ⁣

En esta instalación se podrá apreciar un viaje a través de los vastos paisajes del arte sonoro experimental de América Latina, una exploración que reúne a doce artistas de cuatro colectivos, sellos y agrupaciones que se aventuraron para sumergirse en un proceso de cruce, intercambio y creación colaborativa a distancia.⁣
⁣
Participan: Luciasi Federico Urdinez, Menoko, Facundo Chiesa, Francisco Moraga, Valo Aguilera, Michelle Gutiérrez, Rapo, Murki, Anabelala, TreileManco, Camikant, Malahierba.⁣

La inauguración de SONDA se presenta junto a
PER/SONA una radiopera experimental de @juliangalay
dos instalaciones que podes visitar en ⁣Riobamba 985, 3er piso (CABA) a partir del 1 de febrero.


<hr>
<br>
![01](/assets/images/2024-02-01/flyer.jpg)
