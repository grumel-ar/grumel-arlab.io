---
layout: post
title:  "#0003 - La Disqueria y Amor Loco"
date:   2021-06-23 12:49:10 -0300
categories: post
image: '/assets/images/2021-06-23/01.jpg'
---

## La Disquería | Amor Loco | Cassette

Una escucha de [Ecosistemas del ruido](https://amorloco.club/ediciones/amor-loco-0035-vvaa-ecosistemas-de-ruido/){:target="_blank"}
en [La Disquería](https://www.instagram.com/la_disqueria/){:target="_blank"}

Una edición fisica en cassette del sello [Amor Loco](https://amorloco.club/){:target="_blank"}

### fotis

![01](/assets/images/2021-06-23/01.jpg)
![04](/assets/images/2021-06-23/04.jpg)
![06](/assets/images/2021-06-23/06.jpg)

### video


<video controls>
  <source src="/assets/videos/2021-06-23/01.mp4" type="video/mp4">
</video>
