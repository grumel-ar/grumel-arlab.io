---
layout: page
title: About
permalink: /about/
---
G.R.U.M.E.L - Grupo de Musicas Experimentales Libres

### Propuesta inicial: (7 de Abril 2021)

Hola, aca Rapo. Tengo una idea:
Me surgieron las ganas y el deseo de compartir con personas la escucha de ciertas musicas/obras, de compartir(nos) datas, de intentar generar alguna especie de intercambio/feedback, y porque no de tocar.
Es un montón, y para tratar de que no quede sólo en la mente, escribo estas palabras..
Seria como un grupo de estudio? No lo se. Quizas, no soy bueno nombrando cosas, menos anclando en 1 sola forma.
_Lo que si sé es el ímpetud de pegarle a un platillo y hacer ruidosss_


#### Propuesta incial quizas, entonces, y sin embargo..
Leyendo Oceano de Sonido de D. Toop, un capitulo de los minimalistas de los 60s/70s, como Terry Rilley, La Monte Young, Steve Reich, Philip Glass, Eno... etc. me di cuenta cuanto me cuesta _sentarme a escuchar_ sin ~~hacer~~ nada mas, como me genera imagenes los mantras repetitivos de esos loops, como son otros tiempos (quizas mas lentos) y la imposibilidad (o la falta de praxis) de bajar a palabras la _experiencia_ de escuchar.
También me surgen preguntas: que modos de vivir, modos de estar en el mundo podemos imaginar? que practicas _sonoras pueden ayudarnos_ a realizarlo?

> Nada, eso. Todo, esto.

Disparo esta primer _cosa_ a ver si toma _forma_ de algo. Quizas no lo haga ahora, quizas despues. O quizas nunca.
(claramente pueden ser otras escuchas también! nose, el catálogo de Warp, Dark Ambient, IDM de los 90s, _cualquier cosa_. Solo propongo este inicio...)

<br>
<hr>
<br>
<br>
Tenemos un [grupo publico en telegram](https://t.me/musicas_experimentales_libres)
<br>
Tenemos una [lista de mail](https://we.lurk.org/postorius/lists/grumel.we.lurk.org/)
<br>
Tenemos un [bandcamp](https://grumel.bandcamp.com/)

