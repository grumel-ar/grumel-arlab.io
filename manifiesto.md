---
layout: page
title: Manifiesto
permalink: /manifiesto/
---

![01](/assets/images/GRUMEL_manifiesto.jpg)

Pensamos más en un verbo, una acción, una práctica; menos en un sustantivo, una sustancia, una esencia.

Una _función_: algo que se hace, no importa quién, no importante dónde. Aquí y ahora, una y otra vez, nunca igual. No un producto acabado, sino un proceso interminable.

GRUMEL no "es", sino que "está": _se hace_. Si no se hace, no "es": no existe independientemente de nuestro hacer.

Organizamos eventos. Y pensar el evento, es pensar el pasaje de la potencia al acto.

El evento entonces pasa, llega, es todo. Deja un rastro.

Organizar un evento es como planificar un encuentro, sabiendo que todo pasará sin dudas de otra forma que lo previsto.

Organizar es la práctica de enlazar situaciones siempre singulares.

Y el evento es soportable solo porque hace sentido.

Como hace GRUMEL? no hay 1$ peso, pero si llegara a haber algo lo repartiremos horizontalmente.

Organizar es el arte de suscitar encuentros. No necesariamente entre _lo mismo_ y _lo mismo_, sino entre _lo mismo_ y _lo otro_. Hay acontecimiento político cuando lo mismo se encuentra con lo otro, cuando se transgreden fronteras.

Organizar es poner en circulación. Promover lo común entre diferentes, lo compartido entre singularidades.

Se pueden poner a circular saberes: función de investigación y transmisión. Algo que ha funcionado aquí, se registra y comunica de modo que pueda servir allí. No como receta que imitar, sino como inspiración a recrear. Historias, reflexiones, imágenes, balances de experiencia… Organizarse es también compartir narraciones, construir un fondo común de narraciones a disposición de cualquiera, historias que pueden vincular el pasado y el presente, dos presentes, etc.

Se pueden poner a circular afectos: función de encuentro. Suscitar momentos concretos de cooperación, de fiesta, de pensamiento, de vida compartida. Salir de sí al encuentro de lx otrx, como aventura, como exploración, como viaje, no para captar, convencer o sumar. Encuentros que no tienen que tener necesariamente finalidades a priori, porque del propio encuentro surgen ganas y objetivos nuevos. Encontrarse y ver qué pasa, qué late, qué onda.

Se pueden poner a circular ficciones: función con-fabulatoria. ¿Cómo pueden estar juntxs lxs que no están (físicamente) juntxs? A través de fábulas, ficciones comunes. Si la ideología y la identidad tienden a la rigidez y al cierre, la ficción y la fábula permiten su alteración permanente. Seguir contándose y seguir haciéndose. Pensemos en el nombre GRUMEL: un “paraguas” que permita resonar a muchxs y diferentes, conocerse y reconocerse, sentirse parte de lo mismo sin necesidad de ser idénticos.
La ficción no delimita un adentro y un afuera, sino que es un nombre abierto en el que cualquiera puede contarse. Una contraseña que habilita nuevas complicidades.

GRUMEL deviene en el acronimo Grupo de Musicas Experimentales Libres. Germinó como una situación de diálogo a-dentro de una lista de e-mail que creó rapo. Un primer grupo de personas para probar afinidades-intensidades.

Pero más se trata de un depósito que siempre hay que actualizar mediante una nueva acción; renovarlo, refrescarlo y recrearlo.

<hr>
_remixeado con ideas de Anne Dufourmantelle y Amador Fernández Savater_
